package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;

class ThreadControllerTests {
    private Thread thread;
    private Thread secondThread;
    private ThreadController threadController;
    private Post post;
    private User user;

    @BeforeEach
    @DisplayName("forum creation test")
    void init() {
        threadController = new ThreadController();
        thread = new Thread("test", new Timestamp(123L), "forumTest", "messageTest", "slugTest", "titleTest", 5);
        thread.setId(1);
        secondThread =
                new Thread("test2", new Timestamp(1234L), "forumTest2", "messageTest2", "slugTest2", "titleTest2", 15);
        secondThread.setId(2);
        post = new Post();
        user = new User();
    }


    @Test
    @DisplayName("createPost method")
    void testCreatePost() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
                ResponseEntity result = threadController.createPost("slugTest", List.of(post));
                assertEquals(result.getStatusCode(), HttpStatus.CREATED);
                assertEquals(result.getBody(), List.of(post));
            }
        }
    }

    @Test
    @DisplayName("posts method")
    void testPosts() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
                threadDao.when(() -> ThreadDAO.getPosts(any(), any(), any(), any(), any()))
                        .thenReturn(List.of(post));
                ResponseEntity result = threadController.Posts("slugTest", 1, 2, "asc", true);
                assertEquals(result.getStatusCode(), HttpStatus.OK);
                assertEquals(result.getBody(), List.of(post));
            }
        }
    }

    @Test
    @DisplayName("change method")
    void testChange() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
                threadDao.when(() -> ThreadDAO.getThreadById(anyInt())).thenReturn(secondThread);
                ResponseEntity result = threadController.change("slugTest", secondThread);
                assertEquals(result.getStatusCode(), HttpStatus.OK);
                assertEquals(result.getBody(), secondThread);
            }
        }
    }

    @Test
    @DisplayName("info method")
    void testInfo() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
                ResponseEntity result = threadController.info("slugTest");
                assertEquals(result.getStatusCode(), HttpStatus.OK);
                assertEquals(result.getBody(), thread);
            }
        }
    }

    @Test
    @DisplayName("createVote method")
    void testCreateVote() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
                threadDao.when(() -> ThreadDAO.change(any(), anyInt())).thenReturn(10);
                int voteBefore = thread.getVotes();
                ResponseEntity result = threadController.createVote("slugTest", new Vote("Daniil", 100));
                assertEquals(result.getStatusCode(), HttpStatus.OK);
                assertEquals(result.getBody(), thread);
                int voteAfter = thread.getVotes();
                assertEquals(voteAfter, voteBefore + 100);
            }
        }
    }

}


